﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float MouseSensitivity = 100f;
    public Transform PlayerBody;

    private float xRotation = 0f;

    void Start()
    {
        PlayerBody = GameObject.FindWithTag("Player").GetComponent<Transform>();
        Cursor.lockState = CursorLockMode.Locked;
    }


    void Update()
    {
        // przemnażając z deltaTime usuwamy wpływ ramek wyświetlanych na sekunde na ruszanie kamerą
        float mouseX = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;

        // te linie są po to by można było ograniczyć spojrzenia kursorem między -90 a 90 stopni
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        
        PlayerBody.Rotate(Vector3.up * mouseX);
    }
}
