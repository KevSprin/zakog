﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    public CharacterController Controller;
    public float Speed = 12f;
    public float Gravity = -9.81f;
    public Transform GroundCheck;
    public float GroundDistance = 0.4f;
    public LayerMask GroundMask;
    public float JumpHeight = 3f;
    public GameObject FpsPlayer;
    public Text TankText;
    public float MouseSensitivity = 100f;

    private bool isGrounded;
    private Vector3 velocity;
    private Vector3 fpsPlayerScale;
    private float walkingSpeed;
    private bool inputEnabled;
    private float xRotation = 0f;
    private Camera Cam;
    private GameObject GameManager;
    private PlayerManagement playerManagement;
    private bool isTankNearby;

    void Start()
    {
        Controller = GetComponent<CharacterController>();
        fpsPlayerScale = FpsPlayer.transform.localScale;
        walkingSpeed = Speed / 2;
        inputEnabled = true;
        Cam = transform.Find("FPSCamera").GetComponent<Camera>();
        GameManager = GameObject.Find("GameManager");
        playerManagement = GameManager.GetComponent<PlayerManagement>();
        isTankNearby = false;
    }


    void Update()
    {
        if (inputEnabled)
        {
            isGrounded = Physics.CheckSphere(GroundCheck.position, GroundDistance, GroundMask);
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            // przemnażając z deltaTime usuwamy wpływ ramek wyświetlanych na sekunde na ruszanie kamerą
            float mouseX = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;

            // te linie są po to by można było ograniczyć spojrzenia kursorem między -90 a 90 stopni
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90, 90);
            Cam.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

            transform.Rotate(Vector3.up * mouseX);

            // ------------- Jump
            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(JumpHeight * -2f * Gravity);
            }

            // ------------- sprint
            if (Input.GetKey(KeyCode.LeftShift))
            {
                Speed = walkingSpeed * 2;
            }
            else
            {
                Speed = walkingSpeed;
            }


            Vector3 move = transform.right * x + transform.forward * z;

            Controller.Move(move * Speed * Time.deltaTime);

            // ------------- crouching
            if (Input.GetKey(KeyCode.C))
            {
                FpsPlayer.transform.localScale = new Vector3(fpsPlayerScale.x, fpsPlayerScale.y / 2, fpsPlayerScale.z);
            }
            else
            {
                FpsPlayer.transform.localScale = new Vector3(fpsPlayerScale.x, fpsPlayerScale.y, fpsPlayerScale.z);
            }

            // ------------- moving object
            velocity.y += Gravity * Time.deltaTime;
            Controller.Move(velocity * Time.deltaTime);

            // ------------ Check after tank
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 5.0f, LayerMask.GetMask("Tank")))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                TankText.text = "Press F to mount the TANK!";
                isTankNearby = true;
            }
            else
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                TankText.text = "";
            }
        }
    }

    public bool IsTankNearby()
    {
        return isTankNearby;
    }

    public void ActivateInput()
    {
        inputEnabled = true;
        Cam.enabled = true;
    }

    public void DeactivateInput()
    {
        inputEnabled = false;
        Cam.enabled = false;
    }
}
