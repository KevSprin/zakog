﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class TankController : MonoBehaviour
{
    public float MouseSensitivity = 100f;
    public float RotationSpeed = 50f;
    public float MaxSpeed = 20f;
    public float Acceleration = 5f;

    private Camera Cam;
    private Rigidbody rb;
    private Vector3 eulerLeftAngleVelcity = new Vector3(0, -10, 0);
    private Vector3 eulerRightAngleVelcity = new Vector3(0, 10, 0);
    private bool isBackwards;
    private bool inputEnabled;
    private GameObject gameManager;
    private Transform head;
    private Transform barrel;
    private PlayerManagement playerManagement;
    private float xRotation = 0f;
    private float yRotation = 0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        inputEnabled = false;
        gameManager = GameObject.Find("GameManager");
        playerManagement = gameManager.GetComponent<PlayerManagement>();
        head = transform.Find("Head").transform;
        barrel = head.Find("Barrel").transform;
        Cam = head.Find("TankCamera").GetComponent<Camera>();
        Cam.enabled = false;
    }

    void Update()
    {
        if (inputEnabled)
        {
            float mouseX = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;

           
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -25f, 0);

            yRotation -= mouseY;
            yRotation = Mathf.Clamp(yRotation, -25f, 0);
            Cam.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

            barrel.localRotation = Quaternion.Euler(yRotation, 0f, 0f);

            head.Rotate(Vector3.up * mouseX);
            barrel.Rotate(Vector3.left * mouseY);
            

            if (Input.GetKey(KeyCode.W))
            {
                if (Vector3.Magnitude(rb.velocity) < MaxSpeed)
                    rb.AddForce(transform.forward * Acceleration);
                isBackwards = false;
                eulerLeftAngleVelcity.y = -10f;
                eulerRightAngleVelcity.y = 10f;
            }
            if (Input.GetKey(KeyCode.S))
            {
                if (Vector3.Magnitude(rb.velocity) < (MaxSpeed / 2))
                    rb.AddForce(-transform.forward * Acceleration);
                isBackwards = true;
            }
            if (Input.GetKey(KeyCode.A))
            {
                if (isBackwards) eulerLeftAngleVelcity.y = 10f;
                Quaternion deltaRotation = Quaternion.Euler(eulerLeftAngleVelcity * Time.deltaTime);
                rb.MoveRotation(rb.rotation * deltaRotation);
            }
            if (Input.GetKey(KeyCode.D))
            {
                if (isBackwards) eulerRightAngleVelcity.y = -10f;
                Quaternion deltaRotation = Quaternion.Euler(eulerRightAngleVelcity * Time.deltaTime);
                rb.MoveRotation(rb.rotation * deltaRotation);
            }
        }
    }
    public void ActivateInput()
    {
        inputEnabled = true;
        Cam.enabled = true;
    }

    public void DeactivateInput()
    {
        inputEnabled = false;
        Cam.enabled = false;
    }
}
