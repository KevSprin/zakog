﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManagement : MonoBehaviour
{
    private GameObject Player;
    private GameObject Tank;
    private PlayerControl playerControl;
    private TankController tankControl;
    private bool isMounted;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        Player = GameObject.Find("FPSPlayer");
        Tank = GameObject.Find("Tank");
        playerControl = Player.transform.Find("Cylinder").GetComponent<PlayerControl>();
        tankControl = Tank.transform.GetComponent<TankController>();
        isMounted = false;
    }

    void Update()
    {
        /*if (Input.GetKey(KeyCode.M))
        {
            DismountTank();
        }*/
        if (Input.GetKey(KeyCode.N))
        {
            MountTank();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (playerControl.IsTankNearby() && !isMounted)
                MountTank();
            else if(isMounted)
                DismountTank();
        }
    }

    public void DismountTank()
    {
        playerControl.ActivateInput();
        tankControl.DeactivateInput();
        isMounted = false;
        Player.SetActive(true);
    }

    public void MountTank()
    {
        playerControl.DeactivateInput();
        tankControl.ActivateInput();
        isMounted = true;
        Player.SetActive(false);
    }
}
