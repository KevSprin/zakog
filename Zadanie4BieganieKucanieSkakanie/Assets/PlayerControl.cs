﻿using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public CharacterController Controller;
    public float Speed = 12f;
    public float Gravity = -9.81f;
    public Transform GroundCheck;
    public float GroundDistance = 0.4f;
    public LayerMask GroundMask;
    public float JumpHeight = 3f;
    public GameObject FpsPlayer;

    private bool isGrounded;
    private Vector3 velocity;
    private Vector3 fpsPlayerScale;
    private float walkingSpeed;

    void Start()
    {
        Controller = GetComponent<CharacterController>();
        fpsPlayerScale = FpsPlayer.transform.localScale;
        walkingSpeed = Speed / 2;
    }


    void Update()
    {
        isGrounded = Physics.CheckSphere(GroundCheck.position, GroundDistance, GroundMask);
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(JumpHeight * -2f * Gravity);
        }
        if (Input.GetKey(KeyCode.LeftShift) && isGrounded)
        {
            Speed = walkingSpeed * 2;
        }
        else
        {
            Speed = walkingSpeed;
        }


        Vector3 move = transform.right * x + transform.forward * z;

        Controller.Move(move * Speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.C))
        {
            FpsPlayer.transform.localScale = new Vector3(fpsPlayerScale.x, fpsPlayerScale.y / 2, fpsPlayerScale.z);
        }
        else
        {
            FpsPlayer.transform.localScale = new Vector3(fpsPlayerScale.x, fpsPlayerScale.y, fpsPlayerScale.z);
        }

        velocity.y += Gravity * Time.deltaTime;
        Controller.Move(velocity * Time.deltaTime);
    }
}
