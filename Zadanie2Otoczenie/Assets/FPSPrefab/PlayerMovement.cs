﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController Controller;
    public float Speed = 12f;
    public float gravity = -9.81f;
    public Transform GroundCheck;
    public float GroundDistance = 0.4f;
    public LayerMask GroundMask;
    public float jumpHeight = 3f;

    Vector3 velocity;
    bool isGrounded;

    private void Start()
    {
        Controller.GetComponent<CharacterController>();
    }

    void Update()
    {
        // sprawdzamy czy nasza postać znajduje się na jakiejś warstwie powierzchni GroundMask
        isGrounded = Physics.CheckSphere(GroundCheck.position, GroundDistance, GroundMask);
        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        // określenie kierunku ruchu na podstawie spojrzenia gracza
        Vector3 move = transform.right * x + transform.forward * z;

        Controller.Move(move * Speed * Time.deltaTime);
    
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;
        Controller.Move(velocity * Time.deltaTime);
    }
}
